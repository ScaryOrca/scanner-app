// React, libraries
import React from 'react';
import { css } from 'emotion';

// misc
import * as BRAND from '../../constants/branding';

const List = (props) => {
    return (
        <div className={css`
            margin-bottom: 10px;
        `}>
            <div className={css`
                color: #fff;
                margin-bottom: 5px;
                text-align: left;
            `}>{props.title}</div>
            <ul className={css`
                background: ${BRAND.GRAY_2};
                border: 1px solid #1a1e25;
                border-radius: 10px;
                box-shadow: 0 0 7px rgba(0, 0, 0, .5);
                box-sizing: border-box;
                color: #fff;
                font-size; 20px;
                list-style: none;
                margin: 0;
                padding: 0;
                text-align: left;
            `}>
                {props.children}
            </ul>
        </div>
    );
}

const ListItem = (props) => {
    return (
        <li className={css`
            border-bottom: 1px solid ${BRAND.GRAY_1};
            padding: 8px 10px;
            :last-child {
                border-bottom: none;
            }
        `}>
            <div className={css`
                color: ${BRAND.TEXT_1};
                font-size: 16px;
            `}>{props.title}</div>
            <div className={css`
                font-size: 20px;
            `}>{props.value}</div>
        </li>
    );
}

const ListItemSecurity = (props) => {
    return (
        <li className={css`
            background: linear-gradient(135deg, #ff6969 0%,#f93434 100%);
            border-bottom: 1px solid ${BRAND.GRAY_1};
            border-radius: 10px 10px 0 0 ;
            box-shadow: 0 -3px 15px #f93434;
            color: #9e0000;
            padding: 8px 10px;
        `}>
            <div className={css`
                font-size: 20px;
            `}>{props.value}</div>
        </li>
    );
}


export {
    List, ListItem, ListItemSecurity
}