/* React, libraries */
import React from 'react';
import { css } from 'emotion';

const Table = (props) => {
    return(
        <div>
            <div className={css`
                background: #a3c6ff;
                border: 1px solid #0062ff;
                border-radius: 3px;
                box-sizing: border-box;
                color: #0053d7;
                padding: 5px;
            `}>{props.title}</div>
            {props.children}
        </div>
    );
}

const Field = (props) => {
    return (
        <div className={css`
            border-bottom: 1px solid #eaeaea;
            display: flex;
            justify-content: space-between;
            margin-top: 10px;
        `}>
            <div className={css`
				font-weight: bold;
                text-align: left;
            `}>{props.label}</div>
            <div className={css`
                text-align: right;
            `}>{props.value}</div>
        </div>
    );
}

export {
    Field, Table
}
