import React from 'react';
import { css, cx } from 'emotion';

const PageContent = (props) => {
    return(
        <div id='page-wrapper' className={css`
            background: #393e46;
            border-radius: 10px;
            box-shadow: 0 0 7px rgba(0, 0, 0, .5);
            box-sizing: border-box;
			color: #fafafa;
			flex-grow: 1;
            min-height: 100%;
            margin: 0 auto;
            margin-bottom: 10px;
            padding: 10px;
            text-align: center;
            width: 95%;
        `}>
            {props.children}
        </div>
    );
}

export default PageContent
