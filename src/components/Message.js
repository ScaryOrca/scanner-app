// React, libraries
import React from 'react';
import { css } from 'emotion';

const Message = (props) => {
    return (
        <div onClick={props.onClick} className={css`
            background: #ff6969;
            box-shadow: 0 0 7px rgba(0, 0, 0, .5);
            box-sizing: border-box;
            color: #9e0000;
            padding: 10px;
        `}>
            {props.message}
        </div>
    );
}

export default Message