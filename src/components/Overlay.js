/* React, libraries */
import React from 'react';
import { css } from 'emotion';

/* components */

const Overlay = (props) => {
    return (
        <div className={css`
            align-items: center;
            background: rgba(0, 0, 0, .5);
            color: #fff;
            display: flex;
            flex-direction: column;
            font-size: 36px;
            justify-content: center;
            height: 100%;
            left: 0;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 999;
        `}>
            {props.children}
        </div>
    );
}

export default Overlay