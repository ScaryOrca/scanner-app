import React from 'react';
import { css, cx } from 'emotion';
import * as BRAND from '../constants/branding'

const PageWrapper = (props) => {
    return(
        <div id='page-wrapper' className={css`
            background: #222831;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
            max-height: calc(100vh);
            margin: 0 auto;
            padding: 10px;
            text-align: center;
            width: 100%;
        `}>
            {props.children}
        </div>
    );
}

const PageWrapperAlt = (props) => {
    return(
        <div id='page-wrapper' className={css`
            padding-top: 80px;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
            height: 100%;
            margin: 0 auto;
            text-align: center;
            width: 90%;
        `}>
            {props.children}
        </div>
    );
}
export default PageWrapper
export {
    PageWrapper, PageWrapperAlt
}
