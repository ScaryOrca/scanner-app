import React from 'react';
import { css, cx } from 'emotion';
import * as BRAND from '../constants/branding';

const Header = (props) => {
    return (
        <div id='header'
            className={css`
                background: transparent;
                box-sizing: border-box;
                color: #fff;
                font-size: 36px;
                padding: 10px;
            `}>{props.children}</div>
    );
}

export default Header