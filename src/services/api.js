import { API } from '../constants/urls';

export const authenticate = async (username, password) => {
    const OPTIONS = {
        method: 'POST',
        body: JSON.stringify({
            username,
            password
        })
    }

    const res = await fetch(API['authenticate'], OPTIONS);
    return res.json();
}

export const isSessionActive = async session => {
    const OPTIONS = {
        method: 'GET',
        headers: {
            'X-SPLS-DSID': session
        }
    }

    const res = await fetch(API['sessionStatus'], OPTIONS);
    return res.json(); 
}

export const getItemDetails = async (sku, store, session) => {
    const OPTIONS = {
        method: 'GET',
        headers: {
            'X-SPLS-DSID': session
        }
    }

    const res = await fetch(API['itemDetails'] + '?store=' + store + '&itemcode=' + sku, OPTIONS);
    return res.json();
}

