export const checkSecurityPog = (pog) => {
    switch(pog) {
        case '202':
            return 'Inaccessible over $150';
        case '213':
            return 'Inaccessible over $150 (wrap over $50)';
        case '248':
            return 'Inaccessible';
        case '250':
            return 'Wrap over $50';
        case '266':
            return 'Inaccessible over $80 (boxed)';
        case '269':
            return 'Box over $30 | Mophie empty box';
        case '278':
            return 'Box or wrap over $60';
        case '320':
            return 'Inaccessible over $80 (box over $50)';
        case '342':
            return 'Inaccessible over $60 (box over $40)';
        case '554':
            return 'Cable lock over $80';
        case '650':
            return 'Box over $40';
        case '762':
            return 'Inaccessible over $27';
        default:
            return '';
    }
}

export const checkSecuritySku = () => {

}