/* React, libraries */
import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

/* pages */
import Secure from './pages/Secure';
import Login from './pages/Login';

/* misc */
import { useStore, saveStateLocalStorage, getStateLocalStorage } from './state/store';
import { isSessionActive } from './services/api';
import Overlay from './components/Overlay';
import Camera from './pages/Camera';
import Message from './components/Message';

const App = () => {
    const {state, dispatch} = useStore();

    const [firstLoad, setFirstLoad] = useState(true);

    const checkSession = async (session) => {
        let sessionStatus = await isSessionActive(session);
        if (sessionStatus.statusCode === 200) {
            dispatch({
                type: 'setAuthentication',
                isAuthenticated: true
            });

            dispatch({
                type: 'setMessage',
                message: '',
                messageVisible: false
            });
        } else if (sessionStatus.statusCode === 401) {
            dispatch({
                type: 'setMessage',
                message: 'Expired Session',
                messageVisible: true
            });

            dispatch({
                type: 'setSession',
                session: ''
            });
        }

        dispatch({
            type: 'loading',
            isLoading: false
        });
    }

    const handleMessageDismiss = () => {
        dispatch({
            type: 'setMessage',
            message: '',
            messageVisible: false
        });
    }

    // setup
    useEffect(() => {
        // show loading screen
        dispatch({
            type: 'loading',
            isLoading: true
        });

        // retrieve state from local storage
        let restoredState = getStateLocalStorage(state);

        // reset a few values
        restoredState.isAuthenticated = false;
        restoredState.isLoading = true;
        restoredState.messageVisible = false;
        console.log(restoredState);

        // restore state we just retrieved
        dispatch({
            type: 'restoreState',
            restoredState: restoredState
        });

        if (restoredState.session !== '') {
            checkSession(restoredState.session);
        } else {
            dispatch({
                type: 'loading',
                isLoading: false
            });
        }
        setFirstLoad(false);
    }, []);

    // save state
    useEffect(() => {
        if (!firstLoad) {
            saveStateLocalStorage(state);
            console.log('saving state:' + state);
        }
    }, [state.session, state.sku]);

    return (
        <Router basename={'koala'}>
            {state.isLoading ? <Overlay>Loading...</Overlay> : ''}
            {state.messageVisible ? <Message message={state.message} onClick={() => handleMessageDismiss()} /> : ''}
            <Switch>
                <Route exact path='/'>
                    <Redirect to='/login' />
                </Route>
                <Route path='/secure'>
                    <Secure />
                </Route>
                <Route path='/login'>
                    <Login />
                </Route>
                <Route path='/scan'>
                    <Camera />
                </Route>
            </Switch>
        </Router>
  );
}

export default App;
