/* React, libraries */
import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { css } from 'emotion';
import SwipeableViews from 'react-swipeable-views';

/* secure pages */
import ScanHistory from './secure/ScanHistory';
import Settings from './secure/Settings';
import Details from './secure/item/Details';

/* components */
import Header from '../components/Header';
import PageContent from '../components/structure/PageContent';
import PageWrapper from '../components/PageWrapper';

/* misc */
import { useStore } from '../state/store';

const Secure = () => {
    let history = useHistory();

    const {state, dispatch} = useStore();

    // check if authenticated
    useEffect(() => {
        if (!state.isAuthenticated) {
            history.push('/login');
        }
    }, [state.isAuthenticated]);

    const handleChangeIndex = (index) => {
        dispatch({
            type: 'updatePageIndex',
            pageIndex: index
        });
    }

    return(
        <React.Fragment>
            <Helmet>
                <title>Details</title>
            </Helmet>
            <SwipeableViews index={state.pageIndex} onChangeIndex={(index) => handleChangeIndex(index)} resistance>
                <Settings />
                <ScanHistory />
                <Details />
                <PageWrapper>
                    <Header>History</Header>
                    <PageContent>
                        Content...    
                    </PageContent>
                </PageWrapper>
            </SwipeableViews>
        </React.Fragment>
    );
}

export default Secure