// React, libraries
import React, { useEffect } from 'react';
import { css } from 'emotion';
import Quagga from 'quagga';
import { useHistory } from 'react-router-dom';

// misc
import { useStore } from '../state/store';

const Camera = () => {
    const {state, dispatch} = useStore();
    let history = useHistory();
    
    /* check if authenticated */
    if (!state.isAuthenticated) {
        history.push('/login');
    }

    // start camera
    useEffect(() => {
        initCamera();
    }, []);

    const initCamera = () => {
        Quagga.init({
            inputStream: {
                name: 'Scanner',
                type: 'LiveStream',
                target: document.querySelector('#camera-viewfinder'),
                constraints: {
                    deviceId: state.defaultCamera
                }
            },
            decoder: {
                readers: ['code_128_reader', 'upc_reader']
            }
        }, function(error) {
                if (error) {
                    console.log(error);
                    return;
                }
                Quagga.start();
            });

        Quagga.onDetected(res => {
            if (res.codeResult.code) {
                dispatch({
                    type: 'shouldQuery',
                    query: res.codeResult.code,
                    shouldQuery: true
                });

                try {
                    Quagga.stop(); 
                } catch (error) {
                    console.log(error);
                }

                history.push('/secure');
            }
        });
    }

    return(
        <div id='camera-viewfinder' className={css`
            height: 100%;
            width: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 90;

            video {
                height: 100%;
                object-fit: cover;
                width: 100%;
            }
        `}>

        </div>
    );
}

export default Camera