/* React, libraries */
import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';

/* components */
import Header from '../components/Header';
import PageWrapper from '../components/PageWrapper';
import PageContent from '../components/structure/PageContent';

/* misc */
import { useStore } from '../state/store';
import { authenticate } from '../services/api';

const Login = () => {
    const {state, dispatch} = useStore();
    let history = useHistory();

    /* check if authenticated */
    useEffect(() => {
        if (state.isAuthenticated) {
            history.push('/secure');
        }
    }, [state.isAuthenticated]);

    /* local state */
    const [storeNum, setStoreNum] = useState(state.storeNum);
    const [employeeId, setEmployeeId] = useState(state.employeeId);
    const [password, setPassword] = useState('');

    const handleAuthentication = async (e) => {
        e.preventDefault();

        /* update global state */
        dispatch({
            type: 'loading',
            isLoading: true
        });

        let res = await authenticate(employeeId, password);

        if (res.data !== undefined && res.statusCode === 200) {
            /* update global state */
            dispatch({
                type: 'login', 
                employeeId: employeeId,
                isAuthenticated: true,
                session: res.data.sessionId,
                storeNum: storeNum
            });

            dispatch({
                type: 'setMessage',
                message: '',
                messageVisible: false
            });
        } else {
            dispatch({
                type: 'setMessage',
                message: 'Unable to login!',
                messageVisible: true
            });
        }

        dispatch({
            type: 'loading',
            isLoading: false
        });
        
    }

    return (
        <React.Fragment>
            <Helmet>
                <title>Login</title>
            </Helmet>
            <PageWrapper>
                <Header>Login</Header>
                <PageContent>
                    <form onSubmit={(e) => handleAuthentication(e)}>
                        <input
                            aria-label='Store Number'
                            className='block-input'
                            value={storeNum}
                            placeholder='Store #'
                            onChange={(e) => setStoreNum(e.target.value)}
                            type='number' />
                        <input
                            aria-label='Employee ID'
                            className='block-input'
                            value={employeeId}
                            placeholder='Employee ID'
                            onChange={(e) => setEmployeeId(e.target.value)} 
                            type='number' />
                        <input
                            className='block-input'
                            onChange={(e) => setPassword(e.target.value)} 
                            placeholder='Password'
                            type='password' />
                        <button type='submit'>Login</button>
                    </form>
                </PageContent>
            </PageWrapper>
        </React.Fragment>
    );
}

export default Login
