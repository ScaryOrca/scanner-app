// React, libraries
import React, { useState, useEffect } from 'react';
import { css } from 'emotion';

// components
import Header from '../../components/Header';
import PageWrapper from '../../components/PageWrapper';

// misc
import * as BRAND from '../../constants/branding';
import { useStore } from '../../state/store';

const Settings = () => {

    const {state, dispatch} = useStore();

    const [devices, setDevices] = useState([]);

    const getDevices = () => {
        let cameras = [];
        navigator.mediaDevices.enumerateDevices()
        .then(listDevices => {
            listDevices.forEach(device => {
                cameras = devices;

                // only display cameras, nothing else
                if (device.kind === 'videoinput') {
                    cameras.push(device);
                    setDevices(cameras);
                }
            });
        });
    }

    const allCameras = devices.map(camera => 
        <option key={camera.deviceId} value={camera.deviceId}>{camera.label}</option>
    );

    const handleSelectCamera = (id) => {
        dispatch({
            type: 'setCamera',
            defaultCamera: id
        });
    }

    // setup
    useEffect(() => {
        getDevices();
    }, []);

    return (
        <React.Fragment>
            <PageWrapper>
                <Header>Settings</Header>
                <div className={css`
                    background: ${BRAND.GRAY_2};
                    padding: 10px;
                    border-radius: 10px;
                    box-shadow: 0 0 7px rgba(0, 0, 0, .5);
                    color: #fff;
                `}>
                    <select onChange={(e) => handleSelectCamera(e.target.value)} defaultValue={'op'}>
                        <option value='op' disabled>Select Camera</option>
                        {allCameras}
                    </select>
                </div>
            </PageWrapper>
        </React.Fragment>
    );
}

export default Settings