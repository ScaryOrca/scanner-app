// React, libraries
import React from 'react';
import { css } from 'emotion';

// misc
import { useStore } from '../../state/store';
import * as BRAND from '../../constants/branding';

// components
import PageWrapper from '../../components/PageWrapper';
import Header from '../../components/Header';

const ScanHistory = () => {
    const {state, dispatch} = useStore();

    const handleSelection = (query) => {
        dispatch({
            type: 'shouldQuery',
            query: query,
            shouldQuery: true
        });

        dispatch({
            type: 'updatePageIndex',
            pageIndex: 2
        });
    }
    let scannedItems;
    if (state.previousScans) {
        scannedItems = state.previousScans.map((item) =>
            <div onClick={() => handleSelection(item.sku)} key={item.sku} className={css`
                background: ${BRAND.GRAY_2};
                border-radius: 10px;
                box-shadow: 0 0 7px rgba(0, 0, 0, .5);
                box-sizing: border-box;
                color: #fff;
                margin-bottom: 10px;
                padding: 10px;
                text-align: left;
            `}>
                <div className={css`
                    color: ${BRAND.TEXT_1};
                    font-size: 16px;
                `}>{item.sku}</div>
                <div className={css`
                    font-size: 20px;
                `}>{item.description}</div>
            </div>
        );
    }

    return (
        <PageWrapper>
            <Header>Scanned Items</Header>
            {scannedItems}
        </PageWrapper>
    );
}

export default ScanHistory