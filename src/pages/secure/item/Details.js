// React, libraries
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { css } from 'emotion';

// components
import Header from '../../../components/Header';
import { List, ListItem, ListItemSecurity } from '../../../components/structure/List';
import PageWrapper from '../../../components/PageWrapper';

// misc
import { getItemDetails } from '../../../services/api';
import { useStore } from '../../../state/store';
import { checkSecurityPog } from '../../../services/security';

const Details = () => {
    let history = useHistory();
    const {state, dispatch} = useStore();

    // local state
    const [query, setQuery] = useState('');

    // check if should run query
    useEffect(() => {
        if (state.shouldQuery) {
            getDetails(state.query);
        }
    }, [state.shouldQuery]);

    const handleSubmit = (e) => {
        e.preventDefault();
        e.target[0].blur();

        dispatch({
            type: 'shouldQuery',
            shouldQuery: true,
            query: query
        });
    }

    const getDetails = (query) => {
        const gd = async () => {
            // display loading indicator
            dispatch({
                type: 'loading',
                isLoading: true
            });

            let res = await getItemDetails(query, state.storeNum, state.session);

            // check if sku/upc is valid
            if (res.statusCode === 200) {
                dispatch({
                    type: 'setDetails1',
                    cost: '$' + res.data.costPrice,
                    damaged: res.data.damagedQty,
                    description: res.data.description,
                    disposition: res.data.inventoryMgmt,
                    kioskPrice: 'Loading...',
                    model: res.data.modelNumber,
                    onHand: res.data.onHandQty,
                    pending: res.data.pending,
                    pog: res.data.pogDescription !== '' ? res.data.pogDescription.split(' - ')[0] : 'N/A',
                    prevPrice: '$' + res.data.previousPrice,
                    price: '$' + res.data.pos,
                    promo: res.data.promoLocation !== '' ? res.data.promoLocation : 'N/A',
                    security: '',
                    sku: res.data.sku,
                    status: res.data.status,
                    transit: res.data.inTransit,
                    warehouse: res.data.itemQtyAvailInWhse
                });

                let secStatus = checkSecurityPog(res.data.pogDescription.split('-')[0]);

                // check item security
                if (secStatus !== '') {
                    dispatch({
                        type: 'setSecurity',
                        security: secStatus
                    });
                }

                // update scan history
                dispatch({
                    type: 'updateScanHistory',
                    description: res.data.description, 
                    sku: res.data.sku
                });

                // clear query from search field
                setQuery('');

            } else if (res.statusCode === 400) {
                dispatch({
                    type: 'setMessage',
                    message: 'Invalid SKU/UPC',
                    messageVisible: true
                });
            } else if (res.statusCode === 401) {
                dispatch({
                    type: 'isAuthenticated',
                    isAuthenticated: false
                });
            }

            dispatch({
                type: 'shouldQuery',
                shouldQuery: false
            });

            // remove loading indicator
            dispatch({
                type: 'loading',
                isLoading: false
            });
        }

        gd();
    }

    const handleCamera = () => {
        history.push('/scan');
    }

    return (
        <PageWrapper>
            <Header>Details</Header>
            <form onSubmit={(e) => handleSubmit(e)} className={css`
                display: flex;
                justify-content: space-between;
            `}>
                <input
                    className='block-input'
                    name='sku'
                    onChange={(e) => setQuery(e.target.value)} 
                    placeholder='SKU/UPC'
                    type='number'
                    value={query} />
                <button id='camera-button' type='button' onClick={() => handleCamera()}>Cam</button>
            </form>
            <List title='Main Details'>
                {state.security !== '' && state.storeNum === '1049' && <ListItemSecurity title='Security' value={state.security} />}
                <ListItem title='SKU' value={state.sku} />
                <ListItem title='Description' value={state.description} />
                <ListItem title='Location' value={state.pog} />
                <ListItem title='POS Price' value={state.price} />
                <ListItem title='Status' value={state.status} />
            </List>
            <List title='Location'>
                <ListItem title='POG' value={state.pog} />
                <ListItem title='Promo' value={state.promo} />
            </List>
            <List title='Pricing'>
                <ListItem title='POS Price' value={state.price} />
                <ListItem title='Previous Price' value={state.prevPrice} />
                <ListItem title='Kiosk Price' value={state.kioskPrice} />
                <ListItem title='Cost' value={state.cost} />
            </List>
            <List title='Inventory'>
                <ListItem title='On Hand' value={state.onHand} />
                <ListItem title='Damaged' value={state.damaged} />
                <ListItem title='Pending' value={state.pending} />
                <ListItem title='Transit' value={state.transit} />
                <ListItem title='Warehouse' value={state.warehouse} />
                <ListItem title='Disposition' value={state.disposition} />
            </List>
        </PageWrapper>
    );
}

export default Details