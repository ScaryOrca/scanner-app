export const stateModel = {
    /* misc details */
    defaultCamera: '',
    shouldQuery: false,
    isLoading: false,
    message: '',
    messageVisible: false,
    query: '',
    pageIndex: 2,

    /* session details */
    employeeId: '',
    isAuthenticated: false,
    session: '',
    storeNum: '',

    /* sku details */
    cost: '',
    damaged: '',
    description: '',
    disposition: '',
    facings: '',
    img: '',
    kioskPrice: '',
    model: '',
    onHand: '',
    pending: '',
    pog: '',
    prevPrice: '',
    price: '',
    promo: '',
    security: '',
    sku: '',
    status: '',
    transit: '',
    upc: '',
    warehouse: '',

    /* sku history details */
    detailedHistory: [],
    lastReturn: '',
    lastReturnQty: '',
    lastSale: '',

    /* previous scans */
    previousScans: []
}