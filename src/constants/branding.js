// branding

// colors
export const GRAY_1 = '#222831';
export const GRAY_2 = '#393e46';
export const STAPLES_RED = '#cc0000';
export const TEXT_1 = '#a8aaac';

// gradients
export const GRADIENT_RED = 'linear-gradient(135deg, #ffb5b5 0%, #fc0000 100%)';