export const API = {
    authenticate: 'https://spls-api.dmillerw.me/api/auth/login',
    itemDetails: 'https://spls-api.dmillerw.me/api/merchandising/getItemDetails',
    itemHistory: 'https://spls-api.dmillerw.me/api/merchandising/getItemHistory',
    pogDetails: 'https://spls-api.dmillerw.me/api/planograms/getSkuInfo',
    staplesCom: 'https://getdistro.co/api/v1/staplesWeb',
    sessionStatus: 'https://spls-api.dmillerw.me/api/auth/checkLoginStatus',
    signOptions: 'https://spls-api.dmillerw.me/api/signage/getSignOptions',
    addTag: 'https://spls-api.dmillerw.me/api/signage/addTagToBatch',
    submitBatch: 'https://spls-api.dmillerw.me/api/signage/submitBatch',
    getPullLists: 'https://spls-api.dmillerw.me/api/pulls/getAllPulls',
    addItemPull: 'https://spls-api.dmillerw.me/api/pulls/addItemToPull',
    submitPull: 'https://spls-api.dmillerw.me/api/pulls/submitPull'
};
