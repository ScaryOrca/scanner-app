import React, { createContext, useContext, useReducer } from 'react';
import { stateModel } from '../constants/stateModel';

const StoreContext = createContext();
const initialState = stateModel;

const reducer = (state, action) => {
    switch(action.type) {
        case 'shouldQuery':
            return {
                ...state,
                shouldQuery: action.shouldQuery,
                query: action.query
            }
        case 'loading':
            return {
                ...state,
                isLoading: action.isLoading
            }   
        case 'login':
            return {
                ...state,
                employeeId: action.employeeId,
                isAuthenticated: action.isAuthenticated,
                session: action.session,
                storeNum: action.storeNum
            }
        case 'restoreState':
            return {
                ...action.restoredState
            }
        case 'setAuthentication':
            return {
                ...state,
                isAuthenticated: action.isAuthenticated
            }
        case 'setCamera':
            return {
                ...state,
                defaultCamera: action.defaultCamera
            }
        case 'setDetails1':
            return {
                ...state,
                cost: action.cost,
                damaged: action.damaged,
                description: action.description,
                disposition: action.disposition,
                kioskPrice: action.kioskPrice,
                model: action.model,
                onHand: action.onHand,
                pending: action.pending,
                pog: action.pog,
                prevPrice: action.prevPrice,
                price: action.price,
                promo: action.promo,
                security: action.security,
                sku: action.sku,
                status: action.status,
                transit: action.transit,
                warehouse: action.warehouse
            }
        case 'setMessage':
            return {
                ...state,
                message: action.message,
                messageVisible: action.messageVisible
            }
        case 'setSecurity':
            return {
                ...state,
                security: action.security
            }
        case 'setSession':
            return {
                ...state,
                session: action.session
            }
        case 'updatePageIndex':
            return {
                ...state,
                pageIndex: action.pageIndex
            }
        case 'updateScanHistory':
            let prevScans = state.previousScans;
            const numScans = prevScans.length;
            const index = prevScans.findIndex(i => i.sku === action.sku);

            // only save the last 15 scans
            if (numScans >= 15) {
                prevScans = prevScans.slice(0, 14);
            }

            // check if latest scan is already in history
            if (index > -1) {
                prevScans.splice(index, 1);
            }

            return {
                ...state,
                previousScans: [{sku: action.sku, description: action.description}, ...prevScans]
            }
    }
}

export const StoreProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <StoreContext.Provider value={{dispatch, state}}>
            {children}
        </StoreContext.Provider>
    );
}

export const saveStateLocalStorage = (storeState) => {
    let stringState = JSON.stringify(storeState);
    localStorage.setItem('state', stringState);
}

export const getStateLocalStorage = () => {
    let localState = localStorage.getItem('state');
    return localState ? JSON.parse(localState) : stateModel;
}

export const useStore = () => useContext(StoreContext);